package com.sda.dp.builder.ex3;

/**
 * Created by RENT on 2017-08-08.
 */
public class Main {

    public static void main(String[] args) {
        MailServer server = new MailServer();
        server.connect(new Client("Marian"));
        server.connect(new Client("Michau"));
        server.connect(new Client("Piotrek"));
        server.connect(new Client("Władek"));

        server.sendMessage(MailFactory.createWarningMail("Yeti"), new Client("NoReply"));
    }
}
