package com.sda.dp.builder.ex3;

import java.util.Date;

/**
 * Created by RENT on 2017-08-08.
 */
public class Mail {

    private String tresc, nadawca, nazwaSerwera, nazwaSkrzynki;
    private boolean czySzyfr, isSpam;
    private Type UNKNOWN, OFFER, SOCIAL, NOTIFICATIONS, FORUM;
    private Date dataNadania, dataOdbioru;

    private Mail(String tresc, String nadawca, String nazwaSerwera, String nazwaSkrzynki, boolean czySzyfr, boolean isSpam, Type UNKNOWN, Type OFFER, Type SOCIAL, Type NOTIFICATIONS, Type FORUM, Date dataNadania, Date dataOdbioru) {
        this.tresc = tresc;
        this.nadawca = nadawca;
        this.nazwaSerwera = nazwaSerwera;
        this.nazwaSkrzynki = nazwaSkrzynki;
        this.czySzyfr = czySzyfr;
        this.isSpam = isSpam;
        this.UNKNOWN = UNKNOWN;
        this.OFFER = OFFER;
        this.SOCIAL = SOCIAL;
        this.NOTIFICATIONS = NOTIFICATIONS;
        this.FORUM = FORUM;
        this.dataNadania = dataNadania;
        this.dataOdbioru = dataOdbioru;
    }

    public String getTresc() {
        return tresc;
    }

    public String getNadawca() {
        return nadawca;
    }

    public String getNazwaSerwera() {
        return nazwaSerwera;
    }

    public String getNazwaSkrzynki() {
        return nazwaSkrzynki;
    }

    public boolean isCzySzyfr() {
        return czySzyfr;
    }

    public boolean isSpam() {
        return isSpam;
    }

    public Date getDataNadania() {
        return dataNadania;
    }

    public Date getDataOdbioru() {
        return dataOdbioru;
    }

    public static class Builder{
        private String tresc;
        private String nadawca;
        private String nazwaSerwera;
        private String nazwaSkrzynki;
        private boolean czySzyfr;
        private boolean isSpam;
        private Type unknown;
        private Type offer;
        private Type social;
        private Type notifications;
        private Type forum;
        private Date dataNadania;
        private Date dataOdbioru;

        public Builder setTresc(String tresc) {
            this.tresc = tresc;
            return this;
        }

        public Builder setNadawca(String nadawca) {
            this.nadawca = nadawca;
            return this;
        }

        public Builder setNazwaSerwera(String nazwaSerwera) {
            this.nazwaSerwera = nazwaSerwera;
            return this;
        }

        public Builder setNazwaSkrzynki(String nazwaSkrzynki) {
            this.nazwaSkrzynki = nazwaSkrzynki;
            return this;
        }

        public Builder setCzySzyfr(boolean czySzyfr) {
            this.czySzyfr = czySzyfr;
            return this;
        }

        public Builder setIsSpam(boolean isSpam) {
            this.isSpam = isSpam;
            return this;
        }

        public Builder setUNKNOWN(Type unknown) {
            this.unknown = unknown;
            return this;
        }

        public Builder setOFFER(Type offer) {
            this.offer = offer;
            return this;
        }

        public Builder setSOCIAL(Type social) {
            this.social = social;
            return this;
        }

        public Builder setNOTIFICATIONS(Type notifications) {
            this.notifications = notifications;
            return this;
        }

        public Builder setFORUM(Type forum) {
            this.forum = forum;
            return this;
        }

        public Builder setDataNadania(Date dataNadania) {
            this.dataNadania = dataNadania;
            return this;
        }

        public Builder setDataOdbioru(Date dataOdbioru) {
            this.dataOdbioru = dataOdbioru;
            return this;
        }

        public Mail createMail() {
            return new Mail(tresc, nadawca, nazwaSerwera, nazwaSkrzynki, czySzyfr, isSpam, unknown, offer, social, notifications, forum, dataNadania, dataOdbioru);
        }
    }
}
