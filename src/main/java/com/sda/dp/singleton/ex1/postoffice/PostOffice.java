package com.sda.dp.singleton.ex1.postoffice;

/**
 * Created by amen on 12/2/17.
 */
public class PostOffice {
    private WaitingRoomMachine machine = new WaitingRoomMachine();
    private Reception reception = new Reception();

    public void generateTicketMachine() {
        System.out.println(machine.generateTicket());
    }

    public void generateTicketReception() {
        System.out.println(reception.generateTicket());
    }

}
