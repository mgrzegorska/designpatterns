package com.sda.dp.singleton.ex2.settingsloader;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // punkt 3
        SettingsReader reader = new SettingsReader();
        reader.readSettingsFromFile();

        Game g = new Game();

        Scanner scanner = new Scanner(System.in);
        while (!g.hasEnded()){
            g.nextRound();  // losowanie liczb i znaku
                            // pytanie do użytkownika (podanie liczb i znaku)
            int userResult = scanner.nextInt();     // pobieram wynik
            if(g.validate(userResult)){             // weryfikacja czy podana
                                                    // liczba jest poprawnym wynikiem
                System.out.println("OK!");
            }else{
                System.out.println("NOT OK!");
            }
        }
    }
}
