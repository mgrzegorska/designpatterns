package com.sda.dp.threadpool.factorialcalculator;

/**
 * Created by amen on 12/9/17.
 */
public class RequestFactorial implements Runnable {

    private int number;

    public RequestFactorial(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        long wynik = silniaNieRekurencyjnie(number);
        System.out.println(number + "! = " + wynik);
    }

    private long silniaNieRekurencyjnie(long number) {
        long value = 1;
        for (int i = 1; i <= number; i++) {
            value *= i;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return value;
    }
}
