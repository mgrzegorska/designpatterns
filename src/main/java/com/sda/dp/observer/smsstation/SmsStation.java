package com.sda.dp.observer.smsstation;

import java.util.Observable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by amen on 11/30/17.
 */
public class SmsStation extends Observable implements ISmsSender{

    // Executor - tj. pula wątków (interfejs Executor)
    // Executors - tj. fabryka abstrakcyjna pul wątków. Wytwarza instancje
    // ThreadPoolExecutors
    private Executor anteny = Executors.newFixedThreadPool(5);


    public void addPhone(String number) {
        addObserver(new Phone(number));
    }

    public void sendSmsRequest(String number, String content) {
        SmsRequest request = new SmsRequest(number, content, this);
        anteny.execute(request);
    }

    public void sendSms(Message request){
        setChanged();
        notifyObservers(request);
    }

    private int silniaNieRekurencyjnie(int number){
        int value = 1;
        for (int i = 1; i <= number; i++) {
            value *= i;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    private int silnia(int i) {
        try{
            Thread.sleep(100);
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        if (i < 1)
            return 1;
        else
            return i * silnia(i - 1);
    }
}
