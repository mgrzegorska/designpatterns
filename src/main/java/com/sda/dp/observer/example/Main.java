package com.sda.dp.observer.example;

/**
 * Created by amen on 12/3/17.
 */
public class Main {
    public static void main(String[] args) {
        Restaurant r = new Restaurant();

        r.addEmployee("A");
        r.addEmployee("B");
        r.addEmployee("C");
        r.addEmployee("D");
        r.addEmployee("E");
        r.addEmployee("F");

        r.addBoss();
        r.addBoss();
        r.addBoss();


        r.newOrder();
    }
}
