package com.sda.dp.observer.example;

/**
 * Created by amen on 12/3/17.
 */
public class Boss implements IObserver {

    @Override
    public void update() {
        // powiadomiony
    }

    @Override
    public void update(Object message) {
        System.out.println("Szef powiadomiony o :" + message);
    }
}
