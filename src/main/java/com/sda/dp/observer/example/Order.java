package com.sda.dp.observer.example;

/**
 * Created by amen on 12/3/17.
 */
public class Order {
    private String what;

    public Order(String what) {
        this.what = what;
    }

    @Override
    public String toString() {
        return "Order{" +
                "what='" + what + '\'' +
                '}';
    }
}
