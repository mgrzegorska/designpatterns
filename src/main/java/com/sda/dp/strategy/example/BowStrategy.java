package com.sda.dp.strategy.example;

/**
 * Created by amen on 12/9/17.
 */
public class BowStrategy implements IStrategy {
    @Override
    public void fight() {
        System.out.println("Szczelam łukiem.");
    }
}
