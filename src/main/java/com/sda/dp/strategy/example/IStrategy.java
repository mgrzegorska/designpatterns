package com.sda.dp.strategy.example;

/**
 * Created by amen on 12/9/17.
 */
public interface IStrategy {
    void fight();
}
