package com.sda.dp.strategy.example;

/**
 * Created by amen on 12/9/17.
 */
public class Hero {

    private IStrategy strategy = new BowStrategy();

    public void setStrategy(IStrategy strategy) {
        System.out.println("Zmieniam strategie: " + strategy);
        this.strategy = strategy;
    }

    public void fight() {
        strategy.fight();
    }
}
