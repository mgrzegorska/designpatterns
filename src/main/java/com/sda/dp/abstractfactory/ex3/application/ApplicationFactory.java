package com.sda.dp.abstractfactory.ex3.application;

import com.sda.dp.abstractfactory.ex3.Person;

import java.time.LocalDateTime;
import java.util.List;

public abstract class ApplicationFactory {

    public static Application creatApplication(Person applicant, String content) {
        return new Application(LocalDateTime.now(),
                "Gdańsk",
                applicant,
                content);
    }

    public static Application createConditionalApplication(Person person,
                                                           List<Double> grades,
                                                           String reason) {

        return new ConditionalStayApplication(LocalDateTime.now(),
                "Gdańsk",
                person,
                "Please let me stay!",
                grades,
                reason);
    }


    public static Application createSchoolarshipApplication(Person person,
                                                            List<Double> grades,
                                                            List<String> extracurricularActivities) {

        return new SchoolarshipApplication(LocalDateTime.now(),
                "Gdańsk",
                person,
                "Gimme more money cuz im smart!",
                extracurricularActivities,
                grades);
    }

    public static Application createSocialSchoolarshipApplication(Person person,
                                                                  List<Double> grades,
                                                                  double income) {

        return new SocialSchoolarshipApplication(LocalDateTime.now(),
                "Gdańsk",
                person, "Gimme more money cuz im poor!",
                grades,
                income);
    }

    public static Application createSemesterExtendApplication(Person person,
                                                              String reason) {

        return new SemesterExtendApplication(LocalDateTime.now(),
                "Gdańsk",
                person, "Let me study longer!",
                reason);
    }
}
