package com.sda.dp.abstractfactory.ex3.application;

import com.sda.dp.abstractfactory.ex3.Person;

import java.time.LocalDateTime;

public class Application {
    protected LocalDateTime dateCreation;
    protected String creationLocation;
    protected Person applicant;
    protected String content;

    public Application(LocalDateTime dateCreation, String creationLocation, Person applicant, String content) {
        this.dateCreation = dateCreation;
        this.creationLocation = creationLocation;
        this.applicant = applicant;
        this.content = content;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getCreationLocation() {
        return creationLocation;
    }

    public void setCreationLocation(String creationLocation) {
        this.creationLocation = creationLocation;
    }

    public Person getApplicant() {
        return applicant;
    }

    public void setApplicant(Person applicant) {
        this.applicant = applicant;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
