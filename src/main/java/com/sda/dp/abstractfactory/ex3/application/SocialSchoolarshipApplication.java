package com.sda.dp.abstractfactory.ex3.application;

import com.sda.dp.abstractfactory.ex3.Person;

import java.time.LocalDateTime;
import java.util.List;

public class SocialSchoolarshipApplication extends Application {
    private List<Double> grades;
    private double totalFamilyIncome;

    public SocialSchoolarshipApplication(LocalDateTime dateCreation,
                                         String creationLocation,
                                         Person applicant,
                                         String content,
                                         List<Double> grades,
                                         double totalFamilyIncome) {
        super(dateCreation, creationLocation, applicant, content);
        this.grades = grades;
        this.totalFamilyIncome = totalFamilyIncome;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public double getTotalFamilyIncome() {
        return totalFamilyIncome;
    }

    public void setTotalFamilyIncome(double totalFamilyIncome) {
        this.totalFamilyIncome = totalFamilyIncome;
    }
}
