package com.sda.dp.abstractfactory.ex3.application;

import com.sda.dp.abstractfactory.ex3.Person;

import java.time.LocalDateTime;

public class SemesterExtendApplication extends Application {
    private String reason;

    public SemesterExtendApplication(LocalDateTime dateCreation,
                                     String creationLocation,
                                     Person applicant,
                                     String content,
                                     String reason) {
        super(dateCreation, creationLocation, applicant, content);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
