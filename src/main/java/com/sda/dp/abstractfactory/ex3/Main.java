package com.sda.dp.abstractfactory.ex3;

import com.sda.dp.abstractfactory.ex3.application.ApplicationFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //
        System.out.println("Gimme name:");
        String name = scanner.next();
        System.out.println("Gimme surname:");
        String surname = scanner.next();
        System.out.println("Gimme index:");
        String index = scanner.next();

        //
        Person applicant = new Person(name, surname, index);
        boolean isWorking = true;
        while (isWorking) {
            String command = scanner.nextLine().toLowerCase().trim();

            if (command.equals("quit")) {
                break;
            } else if (command.equals("apply")) {
                System.out.println("Application types: stay/cond/schoolar/social/extend");
                command = scanner.nextLine().toLowerCase().trim();
                if (command.equals("stay")) {
                    conditionalStay(scanner, applicant);
                } else if (command.equals("social")) {
                    socialSchoolarship(scanner, applicant);
                }
                //...
            }


        }
    }

    private static void socialSchoolarship(Scanner scanner, Person applicant) {
        System.out.println("Give me grades separated by commas (ex. '1,2,3,4,5')");
        String[] gradesStrings = scanner.nextLine().toLowerCase().trim().split(",");
        List<Double> grades = new ArrayList<>();

        for (String grade : gradesStrings) {
            grades.add(Double.parseDouble(grade));
        }

        System.out.println("Give me your family income:");
        String incomeString = scanner.nextLine();
        Double income = Double.parseDouble(incomeString);

        System.out.println(ApplicationFactory.createSocialSchoolarshipApplication(applicant, grades, income));
    }

    private static void conditionalStay(Scanner scanner, Person applicant) {
        System.out.println("Give me grades separated by commas (ex. '1,2,3,4,5')");
        String[] gradesStrings = scanner.nextLine().toLowerCase().trim().split(",");
        List<Double> grades = new ArrayList<>();

        for (String grade : gradesStrings) {
            grades.add(Double.parseDouble(grade));
        }

        System.out.println("Give me a reason:");
        String reason = scanner.nextLine();

        System.out.println(
                ApplicationFactory.createConditionalApplication(
                        applicant,
                        grades,
                        reason));
    }
}
