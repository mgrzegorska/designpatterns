package com.sda.dp.abstractfactory.ex1_computer;

/**
 * Created by amen on 8/8/17.
 */
public class MacPC extends AbstractPC {
    private MacPC(String computerName, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(computerName, brand, cpu_power, gpu_power, isOverclocked);


        System.out.println("Stworzono:  " + this.computerName);
    }

    public static AbstractPC createMacPC() {
        return new MacPC("Apple MAC PRO", COMPUTER_BRAND.APPLE, 100, 90, false);
    }

    public static AbstractPC createMacAir() {
        return new MacPC("Apple MacBook Air", COMPUTER_BRAND.APPLE, 200, 2, true);
    }
}
