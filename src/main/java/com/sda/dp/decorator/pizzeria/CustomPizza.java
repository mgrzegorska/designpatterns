package com.sda.dp.decorator.pizzeria;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by amen on 12/6/17.
 */
public class CustomPizza implements IPizza {
    private IPizza pizza;
    private List<String> additionalIngredients = new ArrayList<>();

    public CustomPizza(IPizza pizza, String... additionalIngredients) {
        this.pizza = pizza;
        this.additionalIngredients.addAll(Arrays.asList(additionalIngredients));
    }

//    public CustomPizza addIngredient(String ingredient) {
//        additionalIngredients.add(ingredient);
//        return this;
//    }

    @Override
    public int getPrice() {
        return pizza.getPrice() + additionalIngredients.size() * 3;
    }

    @Override
    public List<String> getIngredients() {
        List<String> newList = new ArrayList<>(additionalIngredients);
        newList.addAll(pizza.getIngredients());
        return newList;
//        return Stream.concat(
//                pizza.getIngredients().stream(), additionalIngredients.stream())
//                .collect(Collectors.toList());
    }
}
