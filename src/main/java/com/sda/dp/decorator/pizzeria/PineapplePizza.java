package com.sda.dp.decorator.pizzeria;

import java.util.List;

/**
 * Created by amen on 12/6/17.
 */
public final class PineapplePizza extends AbstractPizza {
    public PineapplePizza(int price, List<String> ingr) {
        super(price, ingr);
    }
}
