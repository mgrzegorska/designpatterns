package com.sda.dp.decorator.example;

/**
 * Created by amen on 12/6/17.
 */
public class HeroOnAHorse implements ICharacter {

    private ICharacter hero;
    private boolean hasWeapon;

    public HeroOnAHorse(ICharacter hero, boolean hasWeapon) {
        this.hero = hero;
        this.hasWeapon = hasWeapon;
    }

    @Override
    public int getSpeed() {
        return hero.getSpeed() * 2;
    }

    @Override
    public int getHealth() {
        return hero.getHealth() + 100;
    }

    @Override
    public int getAttackPoints() {
        return hero.getAttackPoints() + 30 + (hasWeapon ? 10 : 0);
    }

    @Override
    public int getDefencePoints() {
        return hero.getDefencePoints() + 30;
    }

    @Override
    public String toString() {
        return "HeroOnAHorse{" +
//                "name='" + hero.getName() + '\'' +
                ", health=" + getHealth() +
//                ", mana=" + hero.getMana() +
                ", attackPoints=" + getAttackPoints() +
                ", defencePoints=" + getDefencePoints() +
                '}';
    }
}
