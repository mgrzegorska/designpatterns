package com.sda.dp.decorator.carshop;

/**
 * Created by amen on 12/6/17.
 */
public class TunedCar implements ICar {

    private ICar car;

    private boolean extraCharger = false;
    private boolean extraEngine = false;

    public TunedCar(ICar car, boolean extraCharger, boolean extraEngine) {
        this.car = car;
        //addExtraCharger - .... tylko jeśli hasCharger ( samochodu podstawowego) jest false
        // innymi słowy jeśli nie mamy turbosprężarki to możemy ją dołożyć
        if (car.hasCharger()) //
            this.extraCharger = false; // zwiększa chargerPressure i
            // ustawia hasCharger na true jesli jest na false
        else
            this.extraCharger = extraCharger;
        this.extraEngine = extraEngine;
    }

    @Override
    public double getHorsePower() {
        //(extraCharger ? 1.1 : 1.0) - addExtraCharger - Zwieksza moc o 10%
        //(extraEngine ? 1.2 : 1.0) - addExtraEngine - zwiększa moc o 20%
        return car.getHorsePower() * (extraCharger ? 1.1 : 1.0) * (extraEngine ? 1.2 : 1.0);
    }

    @Override
    public boolean hasCharger() {
        // jeśli jest turbosprężarka w podstawowym samochodzie lub ta dołożona
        return car.hasCharger() || extraCharger; // charger przestawia na true
    }

    @Override
    public double getEngineCapacity() {
        //(extraEngine ? 0.3 : 0.0) - addExtraEngine - zwieksza pojemnosc silnika
        // (nie jest podane o ile)
        return car.getEngineCapacity() + (extraEngine ? 0.3 : 0.0);
    }

    @Override
    public double getChargerPressure() {
        // (extraCharger ? 0.5 : 0.0) - addExtraCharger - zwieksza chargerPressure
        // (nie jest podane o ile)
        return car.getChargerPressure() + (extraCharger ? 0.5 : 0.0); // charger zwieksza pressure
    }

    @Override
    public String toString() {
        return "TunedCar{" +
                "car=" + car +
                ", extraCharger=" + extraCharger +
                ", extraEngine=" + extraEngine +
                ", horsepower=" + getHorsePower() +
                ", enginecapacity=" + getEngineCapacity() +
                ", chargerpressure=" + getChargerPressure() +
                '}';
    }
}
